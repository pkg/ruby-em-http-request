require 'gem2deb/rake/spectask'

EXCLUDE = [
  'spec/client_spec.rb',
  'spec/dns_spec.rb',
  'spec/http_proxy_spec.rb',
  'spec/middleware/oauth2_spec.rb',
  'spec/middleware_spec.rb',
  'spec/multi_spec.rb',
  'spec/redirect_spec.rb',
]

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = Dir['spec/**/*_spec.rb'] - EXCLUDE
end
